package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while(true){
        //Print round number
        System.out.printf("Let's play round %d\n", roundCounter);
        //ask for input
        String humanChoice = humanChoice();
        //choose random input for machine
        String computerChoice = randomChoice();
        String choiceString = String.format("Human chose %s, computer chose %s.", humanChoice, computerChoice);
        //check who won and print result
        if (isWinner(humanChoice, computerChoice)){
            System.out.printf("%s Human wins.\n", choiceString);
            humanScore++;
        }
        else if (isWinner(computerChoice, humanChoice)){
            System.out.printf("%s Computer wins.\n", choiceString);
            computerScore++;
        }
        else{
            System.out.printf("%s It's a tie.\n", choiceString);
        }        

        //print score
        System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);

        //update round counter and ask to play again
        roundCounter++;
        String continueAnswer = continuePlaying();
        if (continueAnswer.equals("n")){
            break;
        }
    }
    System.out.println("Bye bye :)");
    }

    public String humanChoice() {
        while(true) {
        String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
        if(validateInput(humanChoice, rpsChoices)) {
            return humanChoice;
        }
        else {
            System.out.printf("I don't understand %s. Try again\n", humanChoice);
        }
    }
    }

    public String randomChoice() {
        Random choice = new Random();
        return rpsChoices.get(choice.nextInt(rpsChoices.size())); //fra https://www.geeksforgeeks.org/randomly-select-items-from-a-list-in-java/
    }
    
    public String continuePlaying() {
        while(true){
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            List<String> yn = Arrays.asList("y", "n");
            if (validateInput(continueAnswer, yn)){
                return continueAnswer;
            }
            else{
                System.out.printf("I don't understand %s. Try again.", continueAnswer);
            }
        }
    }

    public boolean isWinner(String choice1, String choice2){
        if (choice1.equals("paper")){
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")){
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");
        }
    }


    public boolean validateInput(String input, List<String> validInput) {
        input = input.toLowerCase();
        return validInput.contains(input);
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
